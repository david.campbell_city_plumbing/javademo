package com.example.geometry.shapes;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShapeRepositoryTest {

    @Test
    void testReadWriteShapes() {
        ShapeRepository shapeRepository = new ShapeRepository(new ShapeFactoryService());

        List<Shape> originalShapes = new ArrayList<>();
        originalShapes.add(new Circle(3));
        originalShapes.add(new Square(2));

        shapeRepository.writeShapes(originalShapes);

        List<Shape> readShapes = shapeRepository.readShapes();

        assertEquals(originalShapes.size(), readShapes.size());
        for (int i = 0; i < originalShapes.size(); i++) {
            assertEquals(originalShapes.get(i).calculateArea(), readShapes.get(i).calculateArea());
        }
    }

}