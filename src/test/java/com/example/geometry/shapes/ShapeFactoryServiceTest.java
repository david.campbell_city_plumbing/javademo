package com.example.geometry.shapes;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ShapeFactoryServiceTest {

    private final ShapeFactoryService testService= new ShapeFactoryService();

    @Test
    void testCreateCircle() {
        Shape circle = testService.createCircle(5);
        assertEquals("circle,5.0", circle.toString());
    }

    @Test
    void testCreateSquare() {
        Shape square = testService.createSquare(4);
        assertEquals("square,4.0", square.toString());
    }

    @Test
    void testCreateShapeWithSquare(){
        Shape square = testService.getShape("square", 7);
        assertEquals("square,7.0", square.toString());
    }
    @Test
    void testCreateShapeWithCircle(){
        Shape circle = testService.getShape("circle", 2);
        assertEquals("circle,2.0", circle.toString());
    }
    @Test
    void testCreateShapeWithTriangle(){
        assertThrows(IllegalArgumentException.class, ()-> testService.getShape("triangle", 3,4,5));
    }
}