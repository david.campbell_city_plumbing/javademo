package com.example.geometry.shapes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Repository
public class ShapeRepository {

    private ShapeFactoryService shapeFactoryService;
    private final String csvFilePath = "shapes.csv"; // assuming this file is in the project root

    @Autowired
    public ShapeRepository(ShapeFactoryService shapeFactoryService) {
        this.shapeFactoryService = shapeFactoryService;
    }

    public List<Shape> readShapes() {
        List<Shape> shapes = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(csvFilePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts.length == 2) {
                    String shapeType = parts[0].trim().toLowerCase();
                    double[] parameters = Arrays.stream(parts, 1, parts.length)
                            .mapToDouble(Double::parseDouble)
                            .toArray();

                    shapes.add(shapeFactoryService.getShape(shapeType, parameters));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return shapes;
    }

    public void writeShapes(List<Shape> shapes) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFilePath))) {
            for (Shape shape : shapes) {
                writer.write(shape.toString());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}