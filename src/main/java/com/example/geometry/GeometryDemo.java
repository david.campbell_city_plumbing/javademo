package com.example.geometry;

import com.example.geometry.shapes.Shape;
import com.example.geometry.shapes.ShapeFactoryService;
import com.example.geometry.shapes.ShapeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;

@Service
public class GeometryDemo {


    private final ShapeFactoryService shapeFactoryService;

    private final ShapeRepository shapeRepository;

    @Autowired
    public GeometryDemo(ShapeFactoryService shapeFactoryService, ShapeRepository shapeRepository) {
        this.shapeFactoryService = shapeFactoryService;
        this.shapeRepository = shapeRepository;
    }

    public void demoShapesDoingStuff() {
        System.out.println("--------------------------------------------------------------------");
        // Create some shapes
        Shape circle = shapeFactoryService.createCircle(5);
        Shape square = shapeFactoryService.createSquare(5);

        // Calculate and display areas
        System.out.println("Circle Area: " + circle.calculateArea());
        System.out.println("Square Area: " + square.calculateArea());

        // Write shapes to the CSV file
        shapeRepository.writeShapes(List.of(circle, square));

        // Read shapes from the CSV file and display their areas
        List<Shape> readShapes = shapeRepository.readShapes();
        System.out.println("\nShapes read from CSV file:");
        for (Shape readShape : readShapes) {
            System.out.println("Shape Type: " + readShape.getClass().getSimpleName());
            System.out.println("Area: " + readShape.calculateArea() + "\n");
        }

        //take the sum of the area of all our shapes in a stream

        System.out.println("Sum of shale areas: "+
                readShapes.stream()
                        .mapToDouble(Shape::calculateArea)
                        .sum());


        double areaTotal = 0.0;
        for(int i = 0; i<readShapes.size(); i++){
            areaTotal+=readShapes.get(i).calculateArea();
        }

        // Use another stream operation to filter and display shapes with area greater than 20
        System.out.println("\nShapes with area greater than 20:");
        readShapes.stream()
                .filter(shape -> shape.calculateArea() > 20)
                .forEach(filteredShape -> {
                    System.out.println("Shape Type: " + filteredShape.getClass().getSimpleName());
                    System.out.println("Area: " + filteredShape.calculateArea() + "\n");
                });
        System.out.println("--------------------------------------------------------------------");
    }

}

