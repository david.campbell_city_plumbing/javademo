package com.example.geometry;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class GeometryApplication {

	@Autowired
	private GeometryDemo geometryDemo;

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(GeometryApplication.class, args);
		context.getBean(GeometryDemo.class).demoShapesDoingStuff();


	}

}