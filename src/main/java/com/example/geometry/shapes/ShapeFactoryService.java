package com.example.geometry.shapes;

import com.example.geometry.shapes.Shape;
import org.springframework.stereotype.Service;

@Service
public class ShapeFactoryService {

    public Shape createCircle(double radius) {
        return new Circle(radius);
    }

    public Shape createSquare(double side) {
        return new Square(side);
    }

    public Shape getShape(String name, double... parameters) {


        return ShapeType.valueOf(name.toUpperCase()).createShape(parameters);
    }

    private enum ShapeType {
        CIRCLE {
            @Override
            public Shape createShape(double... parameters) {
                if (parameters.length < 1) {
                    throw new IllegalArgumentException("Circle requires at least one parameter (radius)");
                }
                return new Circle(parameters[0]);
            }
        },
        SQUARE {
            @Override
            public Shape createShape(double... parameters) {
                if (parameters.length < 1) {
                    throw new IllegalArgumentException("Square requires at least one parameter (side)");
                }
                return new Square(parameters[0]);
            }
        };

        public abstract Shape createShape(double... parameters);

    }
}